from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as authViews

from fileshare import views

urlpatterns = [
    #* PUBLC PATHS
    path("", views.index, name="index"),
    path("searchPublic", views.searchPublic, name="searchPublic"),
    path("filePage/<slug:fileOwner>/<slug:fileName>", views.filePage, name="filePage"),

    #* LOGGED IN USER
    path("userFiles", views.userFiles, name="userFiles"),
    path("searchUserFiles", views.searchUserFiles, name="searchUserFiles"),
    path("userFilesAll", views.userFilesAll, name="userFilesAll"),
    path("userFilesStarred", views.userFilesStarred, name="userFilesStarred"),
    path("filesSharedWithUser", views.filesSharedWithUser, name="filesSharedWithUser"),
    
    #* FILE OPERATIONS
    path("starFile/<slug:fileName>", views.starFile, name="starFile"),
    path("unstarFile/<slug:fileName>", views.unstarFile, name="unstarFile"),
    path("deleteUserFile/<slug:fileName>", views.deleteUserFile, name="deleteUserFile"),
    path("confirmDelete/<slug:fileName>", views.confirmDelete, name="confirmDelete"),
    path("makeFilePublic/<slug:fileName>", views.makeFilePublic, name="makeFilePublic"),
    path("confirmPublic/<slug:fileName>", views.confirmPublic, name="confirmPublic"),
    path("makeFilePrivate/<slug:fileName>", views.makeFilePrivate, name="makeFilePrivate"),
    path("confirmDownload/<slug:fileOwner>/<slug:fileName>", views.confirmDownload, name="confirmDownload"),

    #* FORMS
    path("uploadForm", views.uploadForm, name="uploadForm"),
    path("shareForm/<slug:fileName>", views.shareForm, name="shareForm"),
    
    #* USERS
    path("login", views.login, name="login"),
    path("register", views.register, name="register"),
    path('logout', authViews.LogoutView.as_view(template_name='users/logout.html'), name='logout'),

    #* ADMIN PAGE
    path('admin/', admin.site.urls),
]

#* Access to stored files
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
