from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.contrib.auth.models import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from . import models, forms
import datetime

def index(request):
    context = {}
    pageSize = 50
    context["files"] = models.File.objects.all()[:pageSize]
    return render(request, "fileshare/index.html", context)

def searchPublic(request):
    context = {}
    if request.method == 'POST':
        query = request.POST.get("search")
        if not query:
            return render(request, "fileshare/searchPublic.html", context)
        
        results = models.File.objects.filter(name__icontains=query)
        context["results"] = results
    
    return render(request, "fileshare/searchPublic.html", context)

def login(request):
    if request.method == 'POST':
        username = request.POST["username"]
        password = request.POST["password"]
        user = auth.authenticate(
            request,
            username=username,
            password=password
        )
        if user is not None:
            newUser = models.FileUser.objects.get_or_create(
                username=username
            )

            auth.login(request, user)
            messages.info(request, "Successfully logged in!")
            return HttpResponseRedirect(reverse("index"))
        
        else:
            messages.info(request, "Invalid username or password!")
            return HttpResponseRedirect(reverse("login"))

    else:
        return render(request, "fileshare/login.html")

def register(request):
    context = {}
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():

            try:
                form.save()
                messages.info(request, "Succesfully created account. To use, please log in!")
                return HttpResponseRedirect(reverse("index"))

            except ValidationError as e:
                context["error_message"] = "Some form fields are not valid:"
                for field, msgs in e:
                    message = " ".join(msgs)
                    context["error_message"] += f" {field} - {message}"

    else:
        form = UserCreationForm()

    context["form"] = form
    return render(request, "fileshare/register.html", context)

def filePage(request, *, fileOwner, fileName):
    context = {}

    try:
        currentUser = models.FileUser.objects.get(username=request.user.username)
    except:
        currentUser = None

    #* search for file description
    allFiles = models.File.objects.all()
    owner = models.FileUser.objects.all().get(username=fileOwner)
    currentFile = allFiles.get(name=fileName, owner=owner)

    context["currentUser"] = currentUser
    context["file"] = currentFile
    return render(request, "fileshare/filePage.html", context)

def confirmDownload(request, *, fileOwner, fileName):
    context = {}

    try:
        currentUser = models.FileUser.objects.get(username=request.user.username)
    except:
        currentUser = None
    
    allFiles = models.File.objects.all()
    owner = models.FileUser.objects.all().get(username=fileOwner)
    currentFile = allFiles.get(name=fileName, owner=owner)

    if currentUser is not None:
        currentUser.hasDownloadedFile = True
        currentUser.save()
    
    currentFile.downloads += 1
    currentFile.save()

    context["currentUser"] = currentUser
    context["file"] = currentFile
    return render(request, "fileshare/confirmDownload.html", context)

@login_required
def userFiles(request):
    return render(request, "fileshare/userFiles.html")

@login_required
def searchUserFiles(request):
    context = {}
    currentUser = models.FileUser.objects.get(username=request.user.username)
    if request.method == 'POST':
        query = request.POST.get("search")
        if not query:
            return render(request, "fileshare/searchUserFiles.html", context)
        
        resultsUser = currentUser.userFiles.filter(name__icontains=query)
        resultsShared = currentUser.sharedWithUserFiles.filter(name__icontains=query)
        context["currentUser"] = currentUser
        context["resultsUser"] = resultsUser
        context["resultsShared"] = resultsShared
    
    return render(request, "fileshare/searchUserFiles.html", context)

@login_required
def userFilesAll(request):
    context = {}
    currentUser = models.FileUser.objects.get(username=request.user.username)
    context["currentUser"] = currentUser
    context["currentFiles"] = currentUser.userFiles.all().order_by("dateOfUpload")
    return render(request, "fileshare/userFilesAll.html", context)

@login_required
def deleteUserFile(request, *, fileName):
    context = {}
    currentUser = models.FileUser.objects.get(username=request.user.username)
    context["currentUser"] = currentUser
    currentFiles = currentUser.userFiles.all()
    try:
        #* remove file from database (django-cleanup deletes file)
        fileToDelete = currentFiles.get(name=fileName, owner=currentUser)
        fileToDelete.delete()

        #* decrement files amount of user
        currentUser.filesAmount -= 1
        currentUser.save()
        messages.info(request, f"{fileName} successfully deleted")
    except:
        messages.info(request, f"There has been an problem with the deletion")
    context["currentFiles"] = currentFiles    

    return render(request, "fileshare/userFilesAll.html", context)

@login_required
def confirmDelete(request, *, fileName):
    context = {}
    currentUser = models.FileUser.objects.get(username=request.user.username)
    currentFiles = currentUser.userFiles.all()
    fileToDelete = currentFiles.get(name=fileName, owner=currentUser)
    context["currentUser"] = currentUser
    context["currentFiles"] = currentFiles
    context["fileToDelete"] = fileToDelete
    return render(request, "fileshare/confirmDelete.html", context)

@login_required
def starFile(request, *, fileName):
    context = {}

    currentUser = models.FileUser.objects.get(username=request.user.username)
    currentFile = currentUser.userFiles.get(name=fileName)
    currentFile.isStarred = True
    currentFile.save()

    #* pass variables to view
    currentFiles = currentUser.userFiles.all()
    context["currentUser"] = currentUser
    context["currentFiles"] = currentFiles

    messages.info(request, f"Starred {fileName}")
    return render(request, "fileshare/userFilesAll.html", context)

@login_required
def unstarFile(request, *, fileName):
    context = {}

    currentUser = models.FileUser.objects.get(username=request.user.username)
    currentFile = currentUser.userFiles.get(name=fileName)
    currentFile.isStarred = False
    currentFile.save()

    #* pass variables to view
    currentFiles = currentUser.userFiles.all()
    context["currentUser"] = currentUser
    context["currentFiles"] = currentFiles

    messages.info(request, f"{fileName} removed from starred")
    return render(request, "fileshare/userFilesAll.html", context)

@login_required
def makeFilePublic(request, *, fileName):
    context = {}

    currentUser = models.FileUser.objects.get(username=request.user.username)
    currentFile = currentUser.userFiles.get(name=fileName)
    currentFile.isPublic = True
    currentFile.dateOfPublishing = datetime.datetime.today()
    currentFile.save()

    #* pass variables to view
    currentFiles = currentUser.userFiles.all()
    context["currentUser"] = currentUser
    context["currentFiles"] = currentFiles

    messages.info(request, f"{fileName} is now public")
    return render(request, "fileshare/userFilesAll.html", context)

@login_required
def confirmPublic(request, *, fileName):
    context = {}
    currentUser = models.FileUser.objects.get(username=request.user.username)
    currentFiles = currentUser.userFiles.all()
    fileToPublicise = currentFiles.get(name=fileName, owner=currentUser)
    context["currentUser"] = currentUser
    context["currentFiles"] = currentFiles
    context["fileToPublicise"] = fileToPublicise
    return render(request, "fileshare/confirmPublic.html", context)

@login_required
def makeFilePrivate(request, *, fileName):
    context = {}
    currentUser = models.FileUser.objects.get(username=request.user.username)
    currentFile = currentUser.userFiles.get(name=fileName)
    currentFile.isPublic = False
    currentFile.save()
    
    #* pass variables to view
    currentFiles = currentUser.userFiles.all()
    context["currentUser"] = currentUser
    context["currentFiles"] = currentFiles

    messages.info(request, f"{fileName} is now private")
    return render(request, "fileshare/userFilesAll.html", context)

@login_required
def userFilesStarred(request):
    context = {}
    currentUser = models.FileUser.objects.get(username=request.user.username)
    context["currentUser"] = currentUser
    context["currentFiles"] = currentUser.userFiles.all()
    return render(request, "fileshare/userFilesStarred.html", context)

@login_required
def filesSharedWithUser(request):
    context = {}
    currentUser = models.FileUser.objects.get(username=request.user.username)
    context["currentUser"] = currentUser
    context["sharedWithUserFiles"] = currentUser.sharedWithUserFiles.all()
    return render(request, "fileshare/filesSharedWithUser.html", context)

@login_required
def shareForm(request, *, fileName):
    context = {}
    currentUser = models.FileUser.objects.get(username=request.user.username)
    if request.method == 'POST':
        form = forms.ShareForm(request.POST)
        if form.is_valid():
            shareWith = form.cleaned_data["username"]

            #* Can't send to user that doesn't exist
            try:
                sendToUser = models.FileUser.objects.get(username=shareWith)
            except:
                messages.info(request, "Invalid username input!")
                return HttpResponseRedirect(reverse("filesSharedWithUser"))
            
            #* Try search in user files else files are shared with user
            try:
                currentFile = currentUser.userFiles.get(name=fileName)
            except:
                currentFile = currentUser.sharedWithUserFiles.get(name=fileName)

            #* Can't send to self
            #* Can't send back to owner
            if (currentUser.username == shareWith) or (currentFile.owner == sendToUser):
                messages.info(request, "Invalid username input!")
                return HttpResponseRedirect(reverse("filesSharedWithUser"))
            
            #* Add to new users shared list
            sendToUser.sharedWithUserFiles.add(currentFile)

            try:
                sendToUser.full_clean()
                sendToUser.save()
                messages.info(request, f"Succesfully shared {fileName}.")
                return HttpResponseRedirect(reverse("userFilesAll"))

            except ValidationError as e:
                context["error_message"] = "Some form fields are not valid:"
                for field, msgs in e:
                    message = " ".join(msgs)
                    context["error_message"] += f" {field} - {message}"

    else:
        form = forms.ShareForm()

    context["form"] = form
    context["fileName"] = fileName
    return render(request, "fileshare/shareForm.html", context)

@login_required
def uploadForm(request):
    context = {}
    currentUser = models.FileUser.objects.get(username=request.user.username)
    currentFiles = currentUser.userFiles.all()
    if request.method == 'POST':
        form = forms.UploadForm(request.POST, request.FILES)
        if form.is_valid():
            name = form.cleaned_data["name"]

            #* name must be one word
            if " " in name:
                messages.info(request, "File name must be one word! Please use '_' instead of spaces.")
                return HttpResponseRedirect(reverse("userFilesAll"))
            
            #* user files must have unique names
            try:
                currentFiles.get(name=name)
                messages.info(request, f"File name {name} already exists! Please pick a different name.")
                return HttpResponseRedirect(reverse("userFilesAll"))
            except:
                pass

            fileContent = request.FILES["fileContent"]
            description = form.cleaned_data["description"]

            newFile = models.File(
                name=name,
                owner=currentUser,
                fileContent=fileContent,
                description=description,
                dateOfUpload=datetime.datetime.today()
            )

            try:
                newFile.full_clean()
                newFile.save()

                #* add the file to users profile
                currentUser.userFiles.add(newFile)
                currentUser.filesAmount += 1
                currentUser.save()

                messages.info(request, f"{name} successfully uploaded.")
                return HttpResponseRedirect(reverse("userFilesAll"))

            except ValidationError as e:
                context["error_message"] = "Some form fields are not valid:"
                for field, msgs in e:
                    message = " ".join(msgs)
                    context["error_message"] += f" {field} - {message}"

    else:
        form = forms.UploadForm()

    context["form"] = form
    return render(request, "fileshare/uploadForm.html", context)

#! DEBUG PASSWORD testPassword123
